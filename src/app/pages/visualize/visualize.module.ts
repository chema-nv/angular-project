import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VisualizeRoutingModule } from './visualize-routing.module';
import { VisualizeComponent } from './visualize.component';


@NgModule({
  declarations: [
    VisualizeComponent
  ],
  imports: [
    CommonModule,
    VisualizeRoutingModule
  ]
})
export class VisualizeModule { }
